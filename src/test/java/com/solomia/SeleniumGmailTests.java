package com.solomia;

import com.solomia.business.GmailBO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Solomia Lylyk
 * @version 1.0
 */
public class SeleniumGmailTests extends BaseTest {
    /** Logger. Output into the console */
    private static Logger logger = LogManager.getLogger(SeleniumGmailTests.class);
    /** Gmail Business Object */
    private GmailBO gmailBO = new GmailBO();

    /**
     * Test to  compose a message and send it
     */
    @Test
    public void sendMail() {
        logger.info("Send Mail Test");
        gmailBO.skipInitialPage();
        gmailBO.composeAndSendMail(message);
        Assert.assertTrue(gmailBO.checkMessage());
    }
}
