package com.solomia;

import com.solomia.business.MessageBO;
import com.solomia.entity.Message;
import com.solomia.factory.AndroidDriverManager;
import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
   protected AndroidDriver driver;
   private MessageBO messageBo = new MessageBO();
   protected Message message;

    @BeforeMethod
    public void setUp(){
        driver = AndroidDriverManager.getDriver();
        message = messageBo.createMessage();
    }

    @AfterMethod
    public void afterMethod() {
        AndroidDriverManager.quit();
    }
}
