package com.solomia.constants;

public class Constants {
    /** Wait Constants */
    public static final int IMPLICITLY_WAIT = 15;

    /** Message Constants */
    public static final String MESSAGE_SEND_TO = "solomia.lylyk@gmail.com";
    public static final String MESSAGE_SUBJECT = "PageObject";
    public static final String MESSAGE_BODY = "Hi";

    /** Capabilities */
    public static final String PLATFORM_NAME_CAPABILITY = "Android";
    public static final String DEVICE_NAME_CAPABILITY = "Redmi";
    public static final String UDID_CAPABILITY = "5934c459";
    public static final String APP_PACKAGE_CAPABILITY = "com.google.android.gm";
    public static final String APP_ACTIVITY_CAPABILITY = "com.google.android.gm.GmailActivity";
    public static final String COMMAND_TIME_OUT_CAPABILITY = "60";

    private Constants(){}
}
