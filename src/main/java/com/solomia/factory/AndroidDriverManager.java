package com.solomia.factory;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AndroidDriverManager {
    private static AndroidDriver driver = null;

    private AndroidDriverManager(){}

    public static AndroidDriver getDriver() {
        if(driver == null) {
            driver = new AndroidDriverFactory().createAndroidDriver();
        }
        return driver;
    }

    public static void quit() {
        if (driver != null) {
            driver.quit();
        }
    }

    public static void waitUntilVisible(WebElement element) {
        Wait.until(ExpectedConditions.visibilityOf(element));
    }
}
