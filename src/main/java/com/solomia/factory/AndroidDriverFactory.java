package com.solomia.factory;

import com.solomia.capabilities.CapabilitiesFactory;
import com.solomia.utils.properties.ConfigProperties;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.solomia.constants.Constants.IMPLICITLY_WAIT;

public class AndroidDriverFactory {
    private static Logger logger = LogManager.getLogger(AndroidDriverFactory.class);
    private AndroidDriver driver;

    public AndroidDriver createAndroidDriver() {
        try {
            driver = new AndroidDriver(new URL(ConfigProperties.getAppiumServer()), CapabilitiesFactory.getCapabilities());
        } catch (MalformedURLException e) {
            logger.error(e);
        }
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        return driver;
    }
}
