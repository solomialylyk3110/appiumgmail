package com.solomia.entity;

public class Message {
    private String subject;
    private String textBody;
    private String sendTo;

    public Message(String sendTo, String subject, String textBody ) {
        this.subject = subject;
        this.textBody = textBody;
        this.sendTo = sendTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTextBody() {
        return textBody;
    }

    public void setTextBody(String textBody) {
        this.textBody = textBody;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }
}
