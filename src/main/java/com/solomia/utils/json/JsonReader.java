package com.solomia.utils.json;

import com.solomia.utils.properties.ConfigProperties;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class JsonReader {

    public String getElementFromJson(String name) {
        JSONParser parser = new JSONParser();
        JSONObject person = null;
        try {
            person = (JSONObject) parser.parse(new FileReader(ConfigProperties.getJsonUsersPath()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String element = (String) person.get(name);
        return element;
    }
}
