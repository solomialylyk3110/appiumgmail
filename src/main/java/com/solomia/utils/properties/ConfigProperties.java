package com.solomia.utils.properties;

public  class ConfigProperties {
    public static String getEmailLogInURL() {
        return PropertiesReader.getProperty("EmailLogInURL");
    }

    public static String getInboxURL() {
        return PropertiesReader.getProperty("InboxURL");
    }

    public static String getDraftURL() {
        return PropertiesReader.getProperty("DraftURL");
    }

    public static String getDriverPath() {
        return PropertiesReader.getProperty("driverPath");
    }

    public static String getJsonUsersPath() {
        return PropertiesReader.getProperty("jsonUsersPath");
    }

    public static String getAppiumServer() {
        return PropertiesReader.getProperty("AppiumServer");
    }
}
