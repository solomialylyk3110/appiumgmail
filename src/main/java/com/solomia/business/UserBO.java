package com.solomia.business;

import com.solomia.utils.json.JsonReader;

public class UserBO {
    private JsonReader jsonReader = new JsonReader();

    public UserBO() {
    }

    public String getName() {
        return jsonReader.getElementFromJson("name");
    }

    public String getPassword() {
        return jsonReader.getElementFromJson("password");
    }

    public String getLogin() {
        return jsonReader.getElementFromJson("login");
    }
}
