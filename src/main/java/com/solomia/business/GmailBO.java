package com.solomia.business;

import com.solomia.entity.Message;
import com.solomia.pageobject.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.solomia.constants.Constants.*;


public class GmailBO {
    private static Logger logger = LogManager.getLogger(GmailBO.class);
    private GmailComposeMailPage gmailComposeMailPage;
    private GmailMailPage gmailMailPage;
    private GmailInitialPage gmailInitialPage;
    private GmailAccountsPage gmailAccountsPage;
    private GmailMenuPage gmailMenuPage;

    public GmailBO() {
        this.gmailComposeMailPage = new GmailComposeMailPage();
        this.gmailMailPage = new GmailMailPage();
        this.gmailInitialPage = new GmailInitialPage();
        this.gmailAccountsPage = new GmailAccountsPage();
        this.gmailMenuPage = new GmailMenuPage();

    }

    public void skipInitialPage() {
        logger.info("Skip initial page");
        gmailInitialPage.tapGotItButton();
        gmailAccountsPage.tabGoToGmail();
    }

    public void composeAndSendMail(Message message) {
        logger.info("Compose and send mail method");
        gmailMailPage.tabComposeButton();
        gmailComposeMailPage.fillInSendToField(message.getSendTo());
        gmailComposeMailPage.fillInSubjectField(message.getSubject());
        gmailComposeMailPage.fillInBodyTextField(message.getTextBody());
        gmailComposeMailPage.tabSendButton();
    }

    public void checkDraftFolder() {
        gmailMailPage.tabMenuListIcon();
        gmailMenuPage.tabSentIcon();
    }

    public boolean checkMessage() {
        return gmailMailPage.isInformMessageDisplayed();
    }

}
