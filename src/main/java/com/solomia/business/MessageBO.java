package com.solomia.business;

import com.solomia.entity.Message;

import java.util.Random;

import static com.solomia.constants.Constants.*;

public class MessageBO {

    public Message createMessage() {
        return new Message(MESSAGE_SEND_TO, MESSAGE_SUBJECT, generateMessageBody());
    }

    public String generateMessageBody() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }
}
