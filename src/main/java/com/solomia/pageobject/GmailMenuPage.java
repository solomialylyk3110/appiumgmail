package com.solomia.pageobject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailMenuPage {
    @FindBy(xpath = "//android.widget.LinearLayout[8]/android.widget.LinearLayout")
    private WebElement sentIcon;

    @FindBy(xpath = "//android.widget.LinearLayout[9]/android.widget.LinearLayout")
    private WebElement scheduleIcon;

    public void tabSentIcon() {
        sentIcon.click();
    }

    public void tabScheduleIcon() {
        scheduleIcon.click();
    }
}
