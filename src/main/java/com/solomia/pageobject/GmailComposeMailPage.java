package com.solomia.pageobject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailComposeMailPage extends AbstractGmailPage {
    private static Logger logger = LogManager.getLogger(GmailComposeMailPage.class);

    @FindBy(id = "to")
    private WebElement sendToField;

    @FindBy(id = "subject")
    private WebElement subjectField;

    @FindBy(xpath = "(//android.widget.EditText)[2]")
    private WebElement bodyTextField;

    @FindBy(id = "send")
    private WebElement sendButton;

    public void fillInSendToField(String sendTo) {
        logger.info("Fill in the sendTo field");
        sendToField.click();
        sendToField.clear();
        sendToField.sendKeys(sendTo);
    }

    public void fillInSubjectField(String subject) {
        logger.info("Fill in the subject field");
        subjectField.click();
        subjectField.clear();
        subjectField.sendKeys(subject);
    }

    public void fillInBodyTextField(String body) {
        logger.info("Fill in the bodyMessage field");
        bodyTextField.click();
        bodyTextField.clear();
        bodyTextField.sendKeys(body);
    }

    public void tabSendButton() {
        logger.info("Tab on SendButton");
        sendButton.click();
    }
}
