package com.solomia.pageobject;

import com.solomia.factory.AndroidDriverFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GmailMailPage extends AbstractGmailPage{
    private static Logger logger = LogManager.getLogger(GmailMailPage.class);

    @FindBy(id ="compose_button")
    private WebElement composeButton;

    @FindBy(className = "android.widget.ImageButton")
    private WebElement menuListIcon;

    @FindBy(id="description_text")
    private WebElement informMessage;

    public void tabComposeButton() {
        composeButton.click();
    }

    public void tabMenuListIcon() {
        menuListIcon.click();
    }

    public boolean isInformMessageDisplayed() {
        logger.info("verify is inform message displayed");
        return informMessage.isDisplayed();
    }
}
