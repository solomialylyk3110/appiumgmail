package com.solomia.pageobject;

import com.solomia.factory.AndroidDriverManager;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractGmailPage {
    public AbstractGmailPage() {
        PageFactory.initElements(AndroidDriverManager.getDriver(), this);
    }
}
