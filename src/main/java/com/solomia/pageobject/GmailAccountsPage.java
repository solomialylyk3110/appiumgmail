package com.solomia.pageobject;

import com.solomia.pageobject.AbstractGmailPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.solomia.factory.AndroidDriverManager.waitUntilVisible;

public class GmailAccountsPage extends AbstractGmailPage {
   @FindBy(id="action_done")
    private WebElement goToGmailButton;

   @FindBy(id= "owner")
   private WebElement accountField;


    public void tabGoToGmail() {
        waitUntilVisible(accountField);
        goToGmailButton.click();
    }
}
