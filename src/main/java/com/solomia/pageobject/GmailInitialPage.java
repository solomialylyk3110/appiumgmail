package com.solomia.pageobject;

import com.solomia.factory.AndroidDriverManager;
import com.solomia.pageobject.AbstractGmailPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class GmailInitialPage extends AbstractGmailPage {

    @FindBy(id= "welcome_tour_got_it")
    private WebElement gotItButton;

    public GmailInitialPage() {
        PageFactory.initElements(AndroidDriverManager.getDriver(), this);
    }

    public void tapGotItButton() {
        gotItButton.click();
    }
}
